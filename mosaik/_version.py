"""
This module provides convenient access to the version.
"""

VERSION_INFO = (3, 1, 1)
VERSION = '.'.join(map(str, VERSION_INFO))
